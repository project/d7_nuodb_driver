<?php
/**
 * @file
 * migration_fixup.php
 *
 * This tool is used to correct the NuoDB sequences after
 * migrating a drupal database from an non-NuoDB datbase
 * to NuoDB.  You need the NuoDB PHP PDO Driver installed
 * and configured.  Run this tool with the following syntax:
 *
 *    php migration_fixup.php [nuodb-database-name@nuodb-hostname[:nuodb-port]]
 *
 * Example:
 *
 *    php migration_fixup.php test@localhost
 */

$schema = "DRUPAL";
$dbname = $argv[1];
if (empty($dbname)) {
  print "You need to pass the NuoDB database node argument.\n";
  print "example: php migration_fixup.php test@localhost\n";
  die;
}

try {
  $db = new PDO("nuodb:database=$dbname;schema=$schema", "cloud", "user");
  $query = "SELECT TABLENAME, FIELD, DATATYPE, GENERATOR_SEQUENCE " .
    "FROM SYSTEM.FIELDS WHERE SCHEMA='$schema' AND " .
    "GENERATOR_SEQUENCE IS NOT NULL";
  $res = $db->query($query);
  foreach ($res as $row) {
    $tablename = $row['TABLENAME'];
    $field = $row['FIELD'];
    $datatype = $row['DATATYPE'];
    $generator_sequence = $row['GENERATOR_SEQUENCE'];
    $sql_max_seq = "SELECT MAX($field) AS MAX FROM $schema.$tablename";
    $max_seq_res = $db->query($sql_max_seq);
    $max_seq_fetch = $max_seq_res->fetch(PDO::FETCH_ASSOC);
    $max_id = $max_seq_fetch['MAX'];

    $next_id = $max_id + 1;
    $sql_update_seq = 'ALTER SEQUENCE `' . $generator_sequence .
      '` START WITH  ' . $next_id . ';';
    print ("$sql_update_seq\n");
    $stmt = $db->prepare($sql_update_seq);
    $stmt->execute();

    $sql_del_shared_sequences = "delete from SHARED_SEQUENCES;";
    print ("$sql_del_shared_sequences\n");
    $stmt2 = $db->prepare($sql_del_shared_sequences);
    $stmt2->execute();

    $sql_insert_shared_sequences = "insert into SHARED_SEQUENCES default values;";
    print ("$sql_insert_shared_sequences\n");
    $stmt3 = $db->prepare($sql_insert_shared_sequences);
    $stmt3->execute();

  }
}
catch(PDOException $e) {
  echo $e->getMessage();
  echo "\nFailed\n";
  die;
}
$db = NULL;
echo "done\n";
