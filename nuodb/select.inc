<?php

/**
 * @file
 * Select builder for NuoDB database engine.
 */

/**
 * @addtogroup database
 * @{
 */

/**
 * The extender class for Select queries.
 */
class SelectQuery_nuodb extends SelectQuery {

  /**
   * Function replaceFunctionCall.
   *
   * Replaces occurrence of old_function_name with new_function_name.
   * 
   * @param string $input
   *   The input SQL statement.
   *
   * @param string $old_function_name
   *   The function name to look for.
   *
   * @param string $new_function_name
   *   The replacement function name.
   *
   * @param string $new_post_part
   *   An optional "post part" to be appended to the replacement.
   *
   * @return string
   *   The SQL statement, with any replacements.
   */
  protected function replaceFunctionCall($input, $old_function_name, $new_function_name, $new_post_part) {
    do {
      $pos_a = -1;
      $pos_b = -1;
      $pos_c = -1;
      $look = 1;
      $len = strlen($input);
      $oldlen = strlen($old_function_name);
      $i = 0;
      for ($i = 0; $i < $len; ++$i) {
        if ($look == 1) {
          if (strncasecmp(substr($input, $i, 6), $old_function_name, $oldlen) == 0) {
            $pos_a = $i;
          }
        }
        if (($input[$i] === ' ') || ($input[$i] === '(') || ($input[$i] === ')') || ($input[$i] === ',')) {
          $look = 1;
        }
        else {
          $look = 0;
        }
        if ($pos_a !== -1) {
          break;
        }
      }

      if ($pos_a === -1) {
        return $input;
      }

      $paren_nest = 0;
      if ($pos_a !== -1) {
        $i = $pos_a + $oldlen;
        while ($i < $len) {
          if ($input[$i] === '(') {
            if ($paren_nest === 0) {
              $pos_b = $i;
            }
            ++$paren_nest;
          }
          if ($input[$i] === ')') {
            --$paren_nest;
            if ($paren_nest === 0) {
              $pos_c = $i;
            }
          }
          if ($pos_c !== -1) {
            break;
          }
          ++$i;
        }
      }

      if (($pos_b !== -1) && ($pos_c !== -1)) {
        $nstr = substr($input, 0, $pos_a) . $new_function_name .
          substr($input, $pos_b, $pos_c - $pos_b) . ' ' . $new_post_part .
          substr($input, $pos_c);
        $input = $nstr;
      }
      else {
        $pos_a = -1;
      }
    } while ($pos_a !== -1);

    return $input;
  }

  /**
   * Function __toString.
   *
   * @return string
   *   The SQL statement string.
   */
  public function __toString() {
    // For convenience, we compile the query ourselves if the caller forgot
    // to do it. This allows constructs like "(string) $query" to work. When
    // the query will be executed, it will be recompiled using the proper
    // placeholder generator anyway.
    if (!$this->compiled()) {
      $this->compile($this->connection, $this);
    }

    // Create a sanitized comment string to prepend to the query.
    $comments = $this->connection->makeComment($this->comments);

    // SELECT.
    $query = $comments . 'SELECT ';
    if ($this->distinct) {
      $query .= 'DISTINCT ';
    }

    // FIELDS and EXPRESSIONS.
    $fields = array();
    foreach ($this->tables as $alias => $table) {
      if (!empty($table['all_fields'])) {
        $fields[] = $this->connection->escapeTable($alias) . '.*';
      }
    }
    foreach ($this->fields as $alias => $field) {
      $fields[] = (isset($field['table']) ? $this->connection->escapeTable($field['table']) . '.' : '') . $this->connection->escapeField($field['field']) . ' AS ' . $this->connection->escapeAlias($field['alias']);
    }
    foreach ($this->expressions as $alias => $expression) {
      $fields[] = $expression['expression'] . ' AS ' . $this->connection->escapeAlias($expression['alias']);
    }
    $query .= implode(', ', $fields);

    // FROM - We presume all queries have a FROM, as any query that
    // doesn't won't need the query builder anyway.
    $query .= "\nFROM ";
    foreach ($this->tables as $alias => $table) {
      $query .= "\n";
      if (isset($table['join type'])) {
        $query .= $table['join type'] . ' JOIN ';
      }

      // If the table is a subquery, compile it and
      // integrate it into this query.
      if (($table['table'] instanceof SelectQueryInterface) ||
          ($table['table'] instanceof SelectQuery)) {
        // Run preparation steps on this sub-query before converting to string.
        $subquery = $table['table'];
        $subquery->preExecute();
        $table_string = '( ' . (string) $subquery . ' )';
      }
      else {
        $table_string = '{' . $this->connection->escapeTable($table['table']) . '}';
      }

      $query .= $table_string . ' ' . $this->connection->escapeTable($table['alias']);

      if (!empty($table['condition'])) {
        $query .= ' ON ' . $table['condition'];
      }
    }

    // WHERE.
    if (count($this->where)) {
      $query .= "\n WHERE " . $this->where;
    }

    // GROUP BY.
    if ($this->group) {
      $query .= "\n GROUP BY " . implode(', ', $this->group);
    }

    // HAVING.
    if (count($this->having)) {
      $query .= "\n HAVING " . $this->having;
    }

    // ORDER BY.
    if ($this->order) {
      $query .= "\n ORDER BY ";
      $fields = array();
      foreach ($this->order as $field => $direction) {
        $fields[] = $field . ' ' . $direction;
      }
      $query .= implode(', ', $fields);
    }

    // RANGE
    // There is no universal SQL standard for handling range or limit clauses.
    // Fortunately, all core-supported databases use the same range syntax.
    // Databases that need a different syntax can override this method and
    // do whatever alternate logic they need to.
    if (!empty($this->range)) {
      $query .= "\n OFFSET " . (int) $this->range['start'] .
        " FETCH FIRST " . (int) $this->range['length'];
    }

    // UNION is a little odd, as the select queries to combine are passed into
    // this query, but syntactically they all end up on the same level.
    if ($this->union) {
      foreach ($this->union as $union) {
        $query .= ' ' . $union['type'] . ' ' . (string) $union['query'];
      }
    }

    if ($this->forUpdate) {
      $query .= ' FOR UPDATE';
    }

    $new_query = $this->replaceFunctionCall($query, 'LENGTH',
      'CHARACTER_LENGTH', 'USING OCTETS');

    return $new_query;
  }

  /**
   * Overrides SelectQuery::orderBy().
   *
   * NuoDB adheres strictly to the SQL-92 standard and requires that when
   * using DISTINCT or GROUP BY conditions, fields and expressions that are
   * ordered on also need to be selected. This is a best effort implementation
   * to handle the cases that can be automated by adding the field if it is not
   * yet selected.
   *
   * @code
   *   $query = db_select('node', 'n');
   *   $query->join('node_revision', 'nr', 'n.vid = nr.vid');
   *   $query
   *     ->distinct()
   *     ->fields('n')
   *     ->orderBy('timestamp');
   * @endcode
   *
   * In this query, it is not possible (without relying on the schema) to know
   * whether timestamp belongs to node_revisions and needs to be added or
   * belongs to node and is already selected. Queries like this will need to be
   * corrected in the original query by adding an explicit call to
   * SelectQuery::addField() or SelectQuery::fields().
   *
   * Since this has a small performance impact, both by the additional
   * processing in this function and in the database that needs to return the
   * additional fields, this is done as an override instead of implementing it
   * directly in SelectQuery::orderBy().
   * 
   * @param string $field
   *   The field to be ordered by.
   *
   * @param string $direction
   *   The direction of the ordering.
   *
   * @return object
   *   "this" object, as returned by parent::orderBy().
   */
  public function orderBy($field, $direction = 'ASC') {
    // Call parent function to order on this.
    $return = parent::orderBy($field, $direction);

    // If there is a table alias specified, split it up.
    if (strpos($field, '.') !== FALSE) {
      list($table, $table_field) = explode('.', $field);
    }
    // Figure out if the field has already been added.
    foreach ($this->fields as $existing_field) {
      if (!empty($table)) {
        // If table alias is given, check if field and table exists.
        if ($existing_field['table'] == $table && $existing_field['field'] == $table_field) {
          return $return;
        }
      }
      else {
        // If there is no table, simply check if the field exists as a field or
        // an aliased field.
        if ($existing_field['alias'] == $field) {
          return $return;
        }
      }
    }

    // Also check expression aliases.
    foreach ($this->expressions as $expression) {
      if ($expression['alias'] == $field) {
        return $return;
      }
    }

    // If a table loads all fields, it can not be added again. It would
    // result in an ambigious alias error because that field would be loaded
    // twice: Once through table_alias.* and once directly. If the field
    // actually belongs to a different table, it must be added manually.
    foreach ($this->tables as $table) {
      if (!empty($table['all_fields'])) {
        return $return;
      }
    }

    // If $field contains an characters which are not allowed in a field name
    // it is considered an expression, these can't be handeld automatically
    // either.
    if ($this->connection->escapeField($field) != $field) {
      return $return;
    }

    // This is a case that can be handled automatically, add the field.
    $this->addField(NULL, $field);
    return $return;
  }
}

/**
 * @} End of "addtogroup database".
 */
