<?php

/**
 * @file
 * Database schema code for NuoDB
 */

/**
 * @ingroup schemaapi
 * @{
 */

/**
 * Database schema for NuoDB
 */
class DatabaseSchema_nuodb extends DatabaseSchema {

  /**
   * A cache of information about blob columns and sequences of tables.
   *
   * This is collected by DatabaseConnection_nuodb->queryTableInformation()
   * by introspecting the database.
   *
   * @see DatabaseConnection_nuodb->queryTableInformation()
   * @var array
   */
  protected $tableInformation = array();

  /**
   * Determine if the NUODB SYSTEM.DATATYPE is a binary/blob/clob field.
   *
   * @param int datatype
   *   The NuoDB SYSTEM.DATATYPE.
   *
   * @return boolean
   *   Retruns 'TRUE' if the SYSTEM.DATATYPE is a binary/blob/clob field.
   */
  public function isBinaryTypeField($datatype) {
    switch ($datatype) {
      /* These types map to NuoDB's Types/Types.h */
      case 12:
      case 13:
      case 14:
      case 16:
      case 19:
      case 20:
      case 21:
        return TRUE;
    }
    return FALSE;
  }

  /**
   * Fetch the list of blobs and sequences used on a table.
   *
   * We introspect the database to collect the information required by insert
   * and update queries.
   *
   * @param string $table
   *   The non-prefixed name of the table.
   *
   * @return object
   *   An object with two member variables:
   *     - 'blob_fields' that lists all the blob fields in the table.
   *     - 'sequences' that lists the sequences used in that table.
   */
  public function queryTableInformation($table) {

    // Generate a key to reference this table's information on.
    $key = $this->connection->prefixTables('{' . $table . '}');
    if (!strpos($key, '.')) {
      $key = 'public.' . $key;
    }

    if (!isset($this->tableInformation[$key])) {

      // Split the key into schema and table for querying.
      list($schema, $table_name) = explode('.', $key);
      $table_information = (object) array(
        'blob_fields' => array(),
        'sequences' => array(),
      );

      // Don't use {} around SYSTEM.FIELDS table.
      $result = $this->connection->query("SELECT FIELD, DATATYPE, GENERATOR_SEQUENCE FROM SYSTEM.FIELDS WHERE SCHEMA = :schema AND TABLENAME = :table", array(
        ':schema' => 'DRUPAL',
        ':table' => $table_name,
      ));
      foreach ($result as $column) {
        if ($this->isBinaryTypeField($column->datatype)) {
          $table_information->blob_fields[$column->field] = TRUE;
        }
        elseif ($column->generator_sequence != NULL) {
          $table_information->sequences[] = $column->generator_sequence;
        }
      }
      $this->tableInformation[$key] = $table_information;
    }
    return $this->tableInformation[$key];
  }

  /**
   * Fetch the list of CHECK constraints used on a field.
   *
   * We introspect the database to collect the information required by field
   * alteration.
   *
   * @param string $table
   *   The non-prefixed name of the table.
   *
   * @param string $field
   *   The name of the field.
   *
   * @return array
   *   An array of all the checks for the field.
   */
  public function queryFieldInformation($table, $field) {
    $prefix_info = $this->getPrefixInfo($table, TRUE);
    $table_name = $prefix_info['table'];

    $field_information = (object) array(
      'checks' => array(),
    );

    // Don't use {} around SYSTEM.FIELDS table.
    $checks = $this->connection->query("SELECT * FROM SYSTEM.FIELDS WHERE SCHEMA = :schema AND TABLENAME = :table AND FIELD = :field", array(
      ':schema' => 'DRUPAL',
      ':table' => $table_name,
      ':field' => $field));
    $field_information = $checks->fetchCol();

    return $field_information;
  }

  /**
   * Generate SQL to create a new table from a Drupal schema definition.
   *
   * @param string $name
   *   The name of the table to create.
   *
   * @param string $table
   *   A Schema API table definition array.
   *
   * @return array
   *   An array of SQL statements to create the table.
   */
  protected function createTableSql($name, $table) {
    $sql_fields = array();
    $index_fields_array = array();
    foreach ($table['fields'] as $field_name => $field) {
      $sql_fields[] = $this->createFieldSql($field_name, $this->processField($field));
    }

    $sql_keys = array();
    if (isset($table['primary key']) && is_array($table['primary key'])) {
      $primary_key_fields = $this->createKeySql($table['primary key']);
      $sql_keys[] = 'PRIMARY KEY (' . $primary_key_fields . ')';
      array_push($index_fields_array, $primary_key_fields);
    }
    if (isset($table['unique keys']) && is_array($table['unique keys'])) {
      foreach ($table['unique keys'] as $key_name => $key) {
        $sql_keys[] = 'CONSTRAINT ' . $this->prefixNonTable($name, $key_name, 'key') . ' UNIQUE (' . implode(', ', $key) . ')';
        $key_fields = $this->createKeySql($key);
        array_push($index_fields_array, $key_fields);
      }
    }

    $sql = 'CREATE TABLE "{' . $name . '}" ' . "(\n\t";
    $sql .= implode(",\n\t", $sql_fields);
    if (count($sql_keys) > 0) {
      $sql .= ",\n\t";
    }
    $sql .= implode(",\n\t", $sql_keys);
    $sql .= "\n)";
    $statements[] = $sql;

    if (isset($table['indexes']) && is_array($table['indexes'])) {
      foreach ($table['indexes'] as $key_name => $key) {
        $key_fields = $this->createKeySql($key);
        if (!in_array($key_fields, $index_fields_array)) { /* don't allow indexes on the same keys */
           $statements[] = $this->createIndexSql($name, $key_name, $key);
           array_push($index_fields_array, $key_fields);
        }
      }
    }

    return $statements;
  }

  /**
   * Function createFieldSql.
   *
   * Create an SQL string for a field to be used in table creation or
   * alteration.
   *
   * Before passing a field out of a schema definition into this
   * function it has to be processed by _db_process_field().
   *
   * @param string $name
   *   Name of the field.
   *
   * @param string $spec
   *   The field specification, as per the schema data structure format.
   */
  protected function createFieldSql($name, $spec) {
    $sql = '"' . $name . '" ' . $spec['nuodb_type'];

    if (isset($spec['type']) && $spec['type'] == 'serial') {
      unset($spec['not null']);
    }

    if (in_array($spec['nuodb_type'], array('VARCHAR', 'CHAR', 'TINYTEXT',
      'MEDIUMTEXT', 'LONGTEXT', 'TEXT'))) {
      if (isset($spec['length'])) {
        $sql .= '(' . $spec['length'] . ')';
      }
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $sql .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $sql .= ' NOT NULL';
      }
      else {
        $sql .= ' NULL';
      }
    }

    if (!empty($spec['auto_increment'])) {
      $sql .= ' GENERATED BY DEFAULT AS IDENTITY';
    }

    // $spec['default'] can be NULL, so we explicitly check for the key here.
    if (array_key_exists('default', $spec)) {
      if (is_string($spec['default'])) {
        $spec['default'] = "'" . $spec['default'] . "'";
      }
      elseif (!isset($spec['default'])) {
        $spec['default'] = 'NULL';
      }
      $sql .= ' DEFAULT ' . $spec['default'];
    }

    if (empty($spec['not null']) && !isset($spec['default']) && empty($spec['auto_increment'])) {
      $sql .= ' DEFAULT NULL';
    }

    return $sql;

  }

  /**
   * Set database-engine specific properties for a field.
   *
   * @param array $field
   *   A field description array, as specified in the schema documentation.
   */
  protected function processField($field) {
    if (!isset($field['size'])) {
      $field['size'] = 'normal';
    }

    // Set the correct database-engine specific datatype.
    // In case one is already provided, force it to lowercase.
    if (isset($field['nuodb_type'])) {
      $field['nuodb_type'] = drupal_strtolower($field['nuodb_type']);
    }
    else {
      $map = $this->getFieldTypeMap();
      $field['nuodb_type'] = $map[$field['type'] . ':' . $field['size']];
    }

    if (!empty($field['unsigned'])) {
      // Unsigned datatypes are not supported in NuoDB.
      // The NuoDB schema in Drupal creates a check constraint
      // to ensure that a value inserted is >= 0. To provide the extra
      // integer capacity, here, we bump up the column field size.
      if (!isset($map)) {
        $map = $this->getFieldTypeMap();
      }
      switch ($field['nuodb_type']) {
        case 'smallint':
          $field['nuodb_type'] = $map['int:medium'];
          break;

        case 'int':
          $field['nuodb_type'] = $map['int:big'];
          break;

      }
    }
    if (isset($field['type']) && $field['type'] == 'serial') {
      unset($field['not null']);
      unset($field['default']);
      $field['auto_increment'] = TRUE;
    }
    return $field;
  }

  /**
   * Function getFieldTypeMap().
   *
   * This maps a generic data type in combination with its data size
   * to the engine-specific data type.
   * 
   * @return array
   *   A map of database types to schema types.
   */
  public function getFieldTypeMap() {
    // Put :normal last so it gets preserved by array_flip. This makes
    // it much easier for modules (such as schema.module) to map
    // database types back into schema types.
    // $map does not use drupal_static as its value never changes.
    static $map = array(
      'varchar:normal'  => 'VARCHAR',
      'char:normal'     => 'CHAR',

      'text:tiny'       => 'TEXT',
      'text:small'      => 'TEXT',
      'text:medium'     => 'TEXT',
      'text:big'        => 'TEXT',
      'text:normal'     => 'TEXT',

      'serial:tiny'     => 'SMALLINT',
      'serial:small'    => 'SMALLINT',
      'serial:medium'   => 'INTEGER',
      'serial:big'      => 'BIGINT',
      'serial:normal'   => 'INT',

      'int:tiny'        => 'SMALLINT',
      'int:small'       => 'SMALLINT',
      'int:medium'      => 'INTEGER',
      'int:big'         => 'BIGINT',
      'int:normal'      => 'INT',

      'float:tiny'      => 'REAL',
      'float:small'     => 'REAL',
      'float:medium'    => 'REAL',
      'float:big'       => 'DOUBLE PRECISION',
      'float:normal'    => 'REAL',

      'numeric:normal'  => 'DECIMAL',

      'blob:big'        => 'BLOB',
      'blob:normal'     => 'BLOB',

      'datetime:normal' => 'TIMESTAMP',

    );
    return $map;
  }

  /**
   * Function createKeySql().
   *
   * @param array $fields
   *   Array of fields.
   *
   * @return string 
   *   SQL fragment for keys.
   */
  protected function createKeySql($fields) {
    $return = array();
    foreach ($fields as $field) {
      if (is_array($field)) {
        $return[] = '"' . $field[0] . '"(' . $field[1] . ')';
      }
      else {
        $return[] = '"' . $field . '"';
      }
    }
    return implode(', ', $return);
  }


  /**
   * Function renameTable.
   * 
   * @param string $table
   *   Old table name.
   * @param string $new_name
   *   New table name.
   * 
   * @return object
   *   Result of the query to rename the table.
   */
  public function renameTable($table, $new_name) {
    if (!$this->tableExists($table)) {
      throw new SchemaObjectDoesNotExistException(t("Cannot rename @table to @table_new: table @table doesn't exist.", array('@table' => $table, '@table_new' => $new_name)));
    }
    if ($this->tableExists($new_name)) {
      throw new SchemaObjectExistsException(t("Cannot rename @table to @table_new: table @table_new already exists.", array('@table' => $table, '@table_new' => $new_name)));
    }

    $info = $this->getPrefixInfo($new_name);
    return $this->connection->query(
      'RENAME TABLE {' . $table . '} TO "' . $info['table'] . '"');
  }

  /**
   * Function findTables.
   *
   * @param string $table_expression
   *   Table name to find.
   *
   * @return object
   *   Return result of fetch all keyed.
   */
  public function findTables($table_expression) {
    // Retrive the table name and schema.
    $table_info = $this->getPrefixInfo($table_expression, FALSE);

    // Don't use {} around SYSTEM.TABLES table.
    $result = $this->connection->query("SELECT TABLENAME FROM SYSTEM.TABLES WHERE TYPE = 'TABLE' AND SCHEMA = 'DRUPAL' AND TABLENAME LIKE :table_name",
      array(
        ':table_name' => $table_info['table'],
      ));
    return $result->fetchAllKeyed(0, 0);
  }

  /**
   * Function dropTable.
   *
   * @param string $table
   *   Table name to drop.
   *
   * @return bool
   *   Returns FALSE is table does not exist, otherwise return TRUE.
   */
  public function dropTable($table) {
    if (!$this->tableExists($table)) {
      return FALSE;
    }

    $this->connection->query('DROP TABLE {' . $table . '}');
    return TRUE;
  }

  /**
   * Function addField.
   * 
   * @param string $table
   *   The name of the table to modify.
   *
   * @param string $field
   *   The name of the field to add.
   *
   * @param array $spec
   *   An associative array of field specifications.
   *
   * @param array $new_keys
   *   An optional array of new keys to add.
   */
  public function addField($table, $field, $spec, $new_keys = array()) {
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add field %table.%field: table doesn't exist.", array('%field' => $field, '%table' => $table)));
    }
    if ($this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add field %table.%field: field already exists.", array('%field' => $field, '%table' => $table)));
    }

    $fixnull = FALSE;
    if (!empty($spec['not null']) && !isset($spec['default'])) {
      $fixnull = TRUE;
      $spec['not null'] = FALSE;
    }
    $query = 'ALTER TABLE {' . $table . '} ADD COLUMN ';
    $query .= $this->createFieldSql($field, $this->processField($spec));
    $this->connection->query($query);
    if (isset($spec['initial'])) {
      $this->connection->update($table)
        ->fields(array($field => $spec['initial']))
        ->execute();
    }
    if ($fixnull) {
      $this->connection->query("ALTER TABLE {" . $table . "} ALTER $field NOT NULL");
    }
    if (isset($new_keys)) {
      $this->createKeys($table, $new_keys);
    }
  }

  /**
   * Function dropField.
   *
   * @param string $table
   *   Table name to modify.
   *
   * @param string $field
   *   Field name to drop.
   *
   * @return bool
   *   Returns FALSE is table/field does not exist, otherwise return TRUE.
   */
  public function dropField($table, $field) {
    if (!$this->fieldExists($table, $field)) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP COLUMN "' . $field . '"');
    return TRUE;
  }

  /**
   * Function fieldSetDefault.
   *
   * @param string $table
   *   Table name to modify.
   *
   * @param string $field
   *   Field name to modify.
   *
   * @param string $default
   *   Default value.
   */
  public function fieldSetDefault($table, $field, $default) {
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot set default value of field %table.%field: field doesn't exist.", array('%table' => $table, '%field' => $field)));
    }

    if (!isset($default)) {
      $default = 'NULL';
    }
    else {
      $default = is_string($default) ? "'$default'" : $default;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" SET DEFAULT ' . $default);
  }

  /**
   * Function fieldSetNoDefault.
   *
   * @param string $table
   *   Table name to modify.
   *
   * @param string $field
   *   Field name to modify.
   */
  public function fieldSetNoDefault($table, $field) {
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot remove default value of field %table.%field: field doesn't exist.", array('%table' => $table, '%field' => $field)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER COLUMN "' . $field . '" DROP DEFAULT');
  }

  /**
   * Function indexExists.
   *
   * @param string $table
   *   Table name
   *
   * @param string $name
   *   Field name
   *
   * @return bool
   *   Returns TRUE if an index exists for specified table & field.
   */
  public function indexExists($table, $name) {
    // Returns one row for each column in the index. Result is string or FALSE.
    $prefix_info = $this->getPrefixInfo($table, TRUE);
    $table_name = $prefix_info['table'];
    $index_name = $table_name . "_" . $name . "_idx";

    // Don't use {} around SYSTEM.INDEXES table.
    $row = $this->connection->query("SELECT INDEXNAME FROM SYSTEM.INDEXES WHERE TABLENAME = '$table_name' AND INDEXNAME = '$index_name'")->fetchAssoc();
    return isset($row['indexname']);
  }

  /**
   * Helper function: check if a constraint (PK, FK, UK) exists.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of the constraint (typically 'pkey' or '[constraint]_key').
   *
   * @return bool
   *   Return TRUE if the specified constraint exists on the table.
   *   Otherwise return FALSE.
   */
  protected function constraintExists($table, $name) {
    $prefix_info = $this->getPrefixInfo($table, TRUE);
    $table_name = $prefix_info['table'];

    // Don't use {} around SYSTEM.TABLECONSTRAINTS table.
    return (bool) $this->connection->query("SELECT 1 FROM SYSTEM.TABLECONSTRAINTS WHERE TABLENAME = '$table_name' AND CONSTRAINTNAME = '$name'")->fetchField();
  }

  /**
   * Function addPrimaryKey.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param array $fields
   *   An array of field names.
   */
  public function addPrimaryKey($table, $fields) {
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add primary key to table %table: table doesn't exist.", array('%table' => $table)));
    }
    if ($this->constraintExists($table, 'pkey')) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add primary key to table %table: primary key already exists.", array('%table' => $table)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD PRIMARY KEY (' . implode(',', $fields) . ')');
  }

  /**
   * Function dropPrimaryKey.
   *
   * @param string $table
   *   The name of the table.
   *
   * @return bool
   *   Returns FALSE if the primary key does not exist. Otherwise returns TRUE.
   */
  public function dropPrimaryKey($table) {
    if (!$this->constraintExists($table, 'pkey')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT ' . $this->prefixNonTable($table, 'pkey'));
    return TRUE;
  }

  /**
   * Function addUniqueKey.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of unique key.
   *
   * @param array $fields
   *   An array of field names.
   */
  public function addUniqueKey($table, $name, $fields) {
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add unique key %name to table %table: table doesn't exist.", array('%table' => $table, '%name' => $name)));
    }
    if ($this->constraintExists($table, $name . '_key')) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add unique key %name to table %table: unique key already exists.", array('%table' => $table, '%name' => $name)));
    }

    $this->connection->query('ALTER TABLE {' . $table . '} ADD CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '" UNIQUE (' . implode(',', $fields) . ')');
  }

  /**
   * Function dropUniqueKey.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of field.
   *
   * @return bool
   *   Return FALSE if the table does not exist.  Otherwise return TRUE.
   */
  public function dropUniqueKey($table, $name) {
    if (!$this->constraintExists($table, $name . '_key')) {
      return FALSE;
    }

    $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT "' . $this->prefixNonTable($table, $name, 'key') . '"');
    return TRUE;
  }

  /**
   * Function addIndex.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of the index.
   *
   * @param array $fields
   *   An array of field names.
   */
  public function addIndex($table, $name, $fields) {
    if (!$this->tableExists($table)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot add index %name to table %table: table doesn't exist.", array('%table' => $table, '%name' => $name)));
    }
    if ($this->indexExists($table, $name)) {
      throw new DatabaseSchemaObjectExistsException(t("Cannot add index %name to table %table: index already exists.", array('%table' => $table, '%name' => $name)));
    }

    $this->connection->query($this->createIndexSql($table, $name, $fields));
  }

  /**
   * Function dropIndex.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of the field.
   */
  public function dropIndex($table, $name) {
    if (!$this->indexExists($table, $name)) {
      return FALSE;
    }

    $this->connection->query('DROP INDEX ' . $this->prefixNonTable($table, $name, 'idx'));
    return TRUE;
  }

  /**
   * Function changeField.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $field
   *   The name of the field.
   *
   * @param string $field_new
   *   New name for the field.
   *
   * @param array $spec
   *   The array of field sepecifications.
   *
   * @param array $new_keys
   *   An optional array of new keys.
   */
  public function changeField($table, $field, $field_new, $spec, $new_keys = array()) {
    if (!$this->fieldExists($table, $field)) {
      throw new DatabaseSchemaObjectDoesNotExistException(t("Cannot change the definition of field %table.%name: field doesn't exist.", array('%table' => $table, '%name' => $field)));
    }
    if (($field != $field_new) && $this->fieldExists($table, $field_new)) {
      throw new DatabaseSchemaObjectExistsException(t(
      "Cannot rename field %table.%name to %name_new: target field already exists.",
      array('%table' => $table, '%name' => $field, '%name_new' => $field_new)));
    }

    $spec = $this->processField($spec);

    // We need to typecast the new column to best be able to transfer the data
    // Schema_nuodb::getFieldTypeMap() will return possibilities that are not
    // 'cast-able' such as 'serial' - so they need to be casted int instead.
    if (in_array($spec['nuodb_type'], array('serial', 'bigserial', 'numeric'))) {
      $typecast = 'int';
    }
    else {
      $typecast = $spec['nuodb_type'];
    }

    if (in_array($spec['nuodb_type'], array('varchar', 'character', 'text')) && isset($spec['length'])) {
      $typecast .= '(' . $spec['length'] . ')';
    }
    elseif (isset($spec['precision']) && isset($spec['scale'])) {
      $typecast .= '(' . $spec['precision'] . ', ' . $spec['scale'] . ')';
    }

    // Remove old check constraints.
    $field_info = $this->queryFieldInformation($table, $field);

    foreach ($field_info as $check) {
      $this->connection->query('ALTER TABLE {' . $table . '} DROP CONSTRAINT "' . $check . '"');
    }

    // Remove old default.
    $this->fieldSetNoDefault($table, $field);

    $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" TYPE ' . $typecast . ' USING "' . $field . '"::' . $typecast);

    if (isset($spec['not null'])) {
      if ($spec['not null']) {
        $nullaction = 'SET NOT NULL';
      }
      else {
        $nullaction = 'DROP NOT NULL';
      }
      $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" ' . $nullaction);
    }

    if (in_array($spec['nuodb_type'], array('serial', 'bigserial'))) {
      $seq = "{" . $table . "}_" . $field_new . "_seq";
      $this->connection->query("CREATE SEQUENCE " . $seq);
      // Set sequence to maximal field value to not conflict with existing
      // entries.
      $this->connection->query("SELECT setval('" . $seq . "', MAX(\"" . $field . '")) FROM {' . $table . "}");
      $this->connection->query('ALTER TABLE {' . $table . '} ALTER "' . $field . '" SET DEFAULT nextval(\'' . $seq . '\')');
    }

    // Rename the column if necessary.
    if ($field != $field_new) {
      $this->connection->query('ALTER TABLE {' . $table . '} RENAME "' . $field . '" TO "' . $field_new . '"');
    }

    // Add unsigned check if necessary.
    if (!empty($spec['unsigned'])) {
      $this->connection->query('ALTER TABLE {' . $table . '} ADD CHECK ("' . $field_new . '" >= 0)');
    }

    // Add default if necessary.
    if (isset($spec['default'])) {
      $this->fieldSetDefault($table, $field_new, $spec['default']);
    }

    if (isset($new_keys)) {
      $this->createKeys($table, $new_keys);
    }
  }

  /**
   * Function createIndexSql.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $name
   *   The name of the index.
   *
   * @param array $fields
   *   An array of field names.
   *
   * @return string
   *   The SQL statement to create the index.
   */
  public function createIndexSql($table, $name, $fields) {
    $query = 'CREATE INDEX "' . $this->prefixNonTable($table, $name, 'idx') . '" ON {' . $table . '} (';
    $query .= $this->createKeySql($fields) . ')';
    return $query;
  }

  /**
   * Function createKeys.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param array $new_keys
   *   An associative array of new keys.
   */
  protected function createKeys($table, $new_keys) {
    if (isset($new_keys['primary key'])) {
      $this->addPrimaryKey($table, $new_keys['primary key']);
    }
    if (isset($new_keys['unique keys'])) {
      foreach ($new_keys['unique keys'] as $name => $fields) {
        $this->addUniqueKey($table, $name, $fields);
      }
    }
    if (isset($new_keys['indexes'])) {
      foreach ($new_keys['indexes'] as $name => $fields) {
        $this->addIndex($table, $name, $fields);
      }
    }
  }

  /**
   * Retrieve a table or column comment.
   *
   * NuoDB does not support table or column comments.  This function
   * does nothing.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $column
   *   An optional column name.
   *
   * @return object
   *   Returns NULL.
   */
  public function getComment($table, $column = NULL) {
    return NULL;
  }

  /**
   * Function tableExists.
   *
   * @param string $table
   *   The name of the table.
   *
   * @return bool
   *   Return TRUE if the table exists, otherwise return FALSE.
   */
  public function tableExists($table) {
    try {
      $this->connection->queryRange("SELECT 1 FROM {" . $table . "}", 0, 1);
      return TRUE;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  /**
   * Function fieldExists.
   *
   * @param string $table
   *   The name of the table.
   *
   * @param string $column
   *   The name of the column.
   *
   * @return bool
   *   Return TRUE if the table exists, otherwise return FALSE.
   */
  public function fieldExists($table, $column) {
    try {
      $this->connection->queryRange("SELECT $column FROM {" . $table . "}", 0, 1);
      return TRUE;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }
}

/**
 * @} End of "@ingroup schemaapi".
 */
