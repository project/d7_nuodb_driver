<?php

/**
 * @file
 * Database interface code for NuoDB database.
 */

/**
 * @addtogroup database
 * @{
 */

/**
 * Database API class.
 *
 * This class extends the Drupal DatabaseConnection class for NuoDB.
 */
class DatabaseConnection_nuodb extends DatabaseConnection {

  /**
   * Constructor function.
   * 
   * @param array $connection_options
   *   An optional array of connection options.
   */
  public function __construct(array $connection_options = array()) {
    // This driver defaults to transaction support,
    // except if explicitly passed FALSE.
    $this->transactionSupport
      = !isset($connection_options['transactions']) ||
      ($connection_options['transactions'] !== FALSE);

    // Nuodb never supports transactional DDL.
    $this->transactionalDDLSupport = FALSE;

    // Default to TCP connection on port 48004.
    if (empty($connection_options['port'])) {
      $connection_options['port'] = 48004;
    }

    if (empty($connection_options['password'])) {
      $connection_options['password'] = NULL;
    }
    else {
      $connection_options['password']
        = str_replace('\\', '\\\\', $connection_options['password']);
    }

    // Allow PDO options to be overridden.
    $connection_options += array(
      'pdo' => array(),
    );

    $connection_options['pdo'] += array(

      // Convert numeric values to strings when fetching.
      PDO::ATTR_STRINGIFY_FETCHES => TRUE,

      // Workaround until DB-1239 is fixed.
      PDO::ATTR_CASE => PDO::CASE_LOWER,

      PDO::ATTR_AUTOCOMMIT => FALSE,
    );

    $this->connectionOptions = $connection_options;

    $dsn = 'nuodb:database=' . $connection_options['database'] . ';schema=DRUPAL';

    parent::__construct($dsn, $connection_options['username'], $connection_options['password'], $connection_options['pdo']);

    // Execute init_commands -- if any.
    if (isset($connection_options['init_commands'])) {
      $this->exec(implode('; ', $connection_options['init_commands']));
    }

  }

  /**
   * Function prepareQuery.
   *
   * @param string $query
   *   The query string as SQL, with curly-braces surrounding the
   *   table names.
   *
   * @return DatabaseStatementInterface
   *   A PDO prepared statement ready for its execute() method.
   */
  public function prepareQuery($query) {
    return parent::prepareQuery($query);
  }

  /**
   * Escapes a field name string.
   *
   * Force all field names to be strictly alphanumeric-plus-underscore
   * and wrap the field name in backquote '`' characters.
   *
   * @param string $field
   *   The field name to escape.
   *
   * @return string
   *   The sanitized field name string.
   */
  public function escapeField($field) {
    return preg_replace('/[^A-Za-z0-9_.]+/', '', '`' . $field . '`');
  }


  /**
   * Executes a query string against the database.
   *
   * This method provides a central handler for the actual execution of every
   * query.
   *
   * @param object $query
   *   The query to execute. In most cases this will be a string containing
   *   an SQL query with placeholders. An already-prepared instance of
   *   DatabaseStatementInterface may also be passed in order to allow calling
   *   code to manually bind variables to a query. If a
   *   DatabaseStatementInterface is passed, the $args array will be ignored.
   *   It is extremely rare that module code will need to pass a statement
   *   object to this method. 
   * @param array $args
   *   An array of arguments for the prepared statement. If the prepared
   *   statement uses ? placeholders, this array must be an indexed array.
   *   If it contains named placeholders, it must be an associative array.
   * @param array $options
   *   An associative array of options to control how the query is run. See
   *   the documentation for DatabaseConnection::defaultOptions() for details.
   *
   * @return DatabaseStatementInterface
   *   This method will return one of: the executed statement, the number of
   *   rows affected by the query (not the number matched), or the generated
   *   insert IT of the last query, depending on the value of
   *   $options['return']. Typically that value will be set by default or a
   *   query builder and should not be set by a user. If there is an error,
   *   this method will return NULL and may throw an exception if
   *   $options['throw_exception'] is TRUE.
   *
   * @throws PDOException
   */
  public function query($query, array $args = array(), $options = array()) {

    $options += $this->defaultOptions();

    try {
      if ($query instanceof DatabaseStatementInterface) {
        $stmt = $query;
        $stmt->execute(NULL, $options);
      }
      else {
        $this->expandArguments($query, $args);
        $stmt = $this->prepareQuery($query);
        $stmt->execute($args, $options);
      }

      switch ($options['return']) {
        case Database::RETURN_STATEMENT:
          return $stmt;

        case Database::RETURN_AFFECTED:
          return $stmt->rowCount();

        case Database::RETURN_INSERT_ID:
          return $this->lastInsertId();

        case Database::RETURN_NULL:
          return;

        default:
          throw new PDOException('Invalid return directive: ' . $options['return']);

      }
    }
    catch (PDOException $e) {
      if ($options['throw_exception']) {
        // Add additional debug information.
        if ($query instanceof DatabaseStatementInterface) {
          $e->query_string = $stmt->getQueryString();
        }
        else {
          $e->query_string = $query;
        }
        $e->args = $args;
        throw $e;
      }
      return NULL;
    }
  }

  /**
   * Runs a limited-range query on this database object.
   *
   * Use this as a substitute for ->query() when a subset of the query is to be
   * returned. User-supplied arguments to the query should be passed in as
   * separate parameters so that they can be properly escaped to avoid SQL
   * injection attacks.
   *
   * @param string $query
   *   A string containing an SQL query.
   * @param object $from
   *   The first result row to return.
   * @param int $count
   *   The maximum number of result rows to return.
   * @param array $args
   *   An array of values to substitute into the query at placeholder markers.
   * @param array $options
   *   An array of options on the query.
   *
   * @return DatabaseStatementInterface
   *   A database query result resource, or NULL if the query was not executed
   *   correctly.
   */
  public function queryRange($query, $from, $count, array $args = array(), array $options = array()) {
    return $this->query($query . ' OFFSET ' . (int) $from . ' FETCH FIRST ' . (int) $count, $args, $options);
  }

  /**
   * Runs a SELECT query and stores its results in a temporary table.
   *
   * Use this as a substitute for ->query() when the results need to stored
   * in a temporary table. Temporary tables exist for the duration of the page
   * request. User-supplied arguments to the query should be passed in as
   * separate parameters so that they can be properly escaped to avoid SQL
   * injection attacks.
   *
   * Note that if you need to know how many results were returned, you should do
   * a SELECT COUNT(*) on the temporary table afterwards.
   *
   * @param string $query
   *   A string containing a normal SELECT SQL query.
   * @param array $args
   *   An array of values to substitute into the query at placeholder markers.
   * @param array $options
   *   An associative array of options to control how the query is run. See
   *   the documentation for DatabaseConnection::defaultOptions() for details.
   *
   * @return string
   *   The name of the temporary table.
   */
  public function queryTemporary($query, array $args = array(), array $options = array()) {
    $tablename = $this->generateTemporaryTableName();
    $this->query(preg_replace('/^SELECT/i', 'CREATE TEMPORARY TABLE {' . $tablename . '} AS SELECT', $query), $args, $options);
    return $tablename;
  }

  /**
   * Returns the type of database driver.
   */
  public function driver() {
    return 'nuodb';
  }

  /**
   * Returns the name of the PDO driver for this connection.
   */
  public function databaseType() {
    return 'nuodb';
  }

  /**
   * Gets any special processing requirements for the condition operator.
   *
   * Some condition types require special processing. This is a simple
   * overridable lookup function. Database connections should define only
   * those operators they wish to be handled differently than the default.
   *
   * @param string $operator
   *   The condition operator. Case-sensitive.
   *
   * @return string
   *   The extra handling directives for the specified operator, or NULL.
   *
   * @see DatabaseCondition::compile()
   */
  public function mapConditionOperator($operator) {
    // We don't want to override any of the defaults.
    // Workaround defect in PHP parameter parser where pdo_parse_params()
    // cannot properly parse SQL statements that contain:
    //
    // ESCAPE '\'
    //
    // pdo_parse_params is doing the wrong thing at the backslash character.
    // (see DB-4594 for more information).
    // Fortunately, '\' is the default ESCAPSE character for LIKE processing
    // in NuoDB so we can just set the 'postfix' to an empty string.
    static $specials = array(
      'LIKE' => array('postfix' => ""),
      'NOT LIKE' => array('postfix' => ""),
      '!=' => array('operator' => '<>'),
    );
    return isset($specials[$operator]) ? $specials[$operator] : NULL;
  }

  /**
   * Retrieves an unique id from a given sequence.
   *
   * @param int $existing_id
   *   After a database import, it might be that the sequences table is behind,
   *   so by passing in the maximum existing id, it can be assured that we
   *   never issue the same id.
   *
   * @return int
   *   An integer number larger than any number returned by earlier calls and
   *   also larger than the $existing_id if one was passed in.
   */
  public function nextId($existing_id = 0) {
    // Transaction is committed when it goes out of scope.
    $transaction = $this->startTransaction();
    $stmt = $this->query('SELECT value FROM {sequences}');
    $curr_id = $stmt->fetchField();
    if (!$curr_id) {
      $this->query('INSERT INTO {sequences} (value) VALUES (:existing_id + 1)', array(
          ':existing_id' => $existing_id,
      ));
      return $this->query('SELECT value FROM {sequences}')->fetchField();
    }
    if ($existing_id > $curr_id) {
      $new_id = $existing_id + 1;
    }
    else {
      $new_id = $curr_id + 1;
    }
    $stmt = $this->query('UPDATE {sequences} SET value = :new_id', array(
      ':new_id' => $new_id,
      ));
    return "$new_id";
  }

  /**
   * Function popCommittableTransactions.
   *
   * Override default popCommittableTransactions function because NuoDB 
   * does not support Transactional DDL.
   */
  protected function popCommittableTransactions() {
    // Commit all the committable layers.
    foreach (array_reverse($this->transactionLayers) as $name => $active) {
      // Stop once we found an active transaction.
      if ($active) {
        break;
      }

      // If there are no more layers left then we should commit.
      unset($this->transactionLayers[$name]);
      if (empty($this->transactionLayers)) {
        if (!PDO::commit()) {
          throw new TransactionCommitFailedException();
        }
      }
      else {
        // Attempt to release this savepoint in the standard way.
        try {
          $this->query('RELEASE SAVEPOINT ' . $name);
        }
        catch (DatabaseExceptionWrapper $e) {
          // However, in Nuodb, savepoints are automatically committed
          // when tables are altered or created (DDL transactions are not
          // supported). This can cause exceptions due to trying to release
          // savepoints which no longer exist.
          //
          // To avoid exceptions when no actual error has occurred, we silently
          // succeed for Nuodb error code 1305 ("SAVEPOINT does not exist").
          if ($e->getPrevious()->errorInfo[1] == '1305') {
            // If one SAVEPOINT was released automatically, then all were.
            // Therefore, clean the transaction stack.
            $this->transactionLayers = array();
            // We also have to explain to PDO that the transaction stack has
            // been cleaned-up.
            PDO::commit();
          }
          else {
            throw $e;
          }
        }
      }
    }
  }


}

/**
 * @} End of "addtogroup database".
 */
