<?php
/**
 * @file
 * Install functions for NuoDB 
 */

class DatabaseTasks_nuodb extends DatabaseTasks {

  protected $pdoDriver = 'nuodb';

  /**
   * Constructor.
   */
  public function __construct() {
    $this->tasks[] = array(
      'function' => 'checkPHPVersion',
      'arguments' => array(),
    );
    $this->tasks[] = array(
      'function' => 'initializeDatabase',
      'arguments' => array(),
    );
  }

  /**
   * Driver name.
   */
  public function name() {
    return st('NuoDB');
  }


  /**
   * Minimum Version.
   */
  public function minimumVersion() {
    return NULL;
  }

  /**
   * Check PHP version.
   */
  public function checkPHPVersion() {
    if (!version_compare(PHP_VERSION, '5.3', '>=')) {
      $this->fail(st('Unsupported version of PHP. You need to upgrade PHP to 5.3 or greater.'));
    };
  }

  /**
   * Make NuoDB Drupal friendly.
   */
  public function initializeDatabase() {
  }
}
