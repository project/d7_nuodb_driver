
-- SUMMARY --

Drupal 7 Database Driver for NuoDB (BETA RELEASE)

-- REQUIREMENTS --

PHP 5.3, 5.4, or 5.5
NuoDB version 2.0.3 or higher
NuoDB PHP PDO Driver
Running NuoDB Database

-- INSTALLATION --

Recursively copy the nuodb directory into <path-to-drupal7>/includes/database.

-- CONFIGURATION --

The NuoDB Drupal Driver depends on a properly configured NuoDB PHP PDO Driver.

Drupal requires a transaction isolation level of READ COMMITTED.  NuoDB has a 
default transaction isolation level of CONSISTENT READ.  To set your Drupal 
site to use the NuoDB READ COMMITED transaction isolation level, you should 
edit your sites 'settings.php' file to contain:

$databases['default']['default']['init_commands'] = array(
  'isolation' => "SET ISOLATION LEVEL READ COMMITTED;"
);


When using the "DBTNG Migrator" tool to migrate other database(s) to NuoDB, 
you should run the migration_fixup.php script to fix up the NuoDB sequences.
Look at the top of migration_fixup.php for more information.

-- TROUBLESHOOTING --

Verify that your PHP configuration shows the NuoDB PDO Driver.


-- CONTACT --

Current maintainers:
* Tom Gates (NuoDB) - tgates@nuodb.com
